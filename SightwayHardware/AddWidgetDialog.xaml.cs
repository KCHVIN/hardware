﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Boîte de dialogue de contenu, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace SightwayHardware
{
    public sealed partial class AddWidgetDialog : ContentDialog
    {
        public AddWidgetDialog()
        {
            this.InitializeComponent();
        }

        public int X
        {
            get 
            {
                return int.Parse(TextInputDialogX.Text); 
            }
        }

        public int Y
        {
            get
            {
                return int.Parse(TextInputDialogY.Text);
            }
        }
    }
}
