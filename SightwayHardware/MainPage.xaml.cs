﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SightwayHardware
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    /// 
    

    public sealed partial class MainPage : Page
    {
        int WidgetX;
        int WidgetY;
        Widget[] Widgets;
        public MainPage()
        {
            this.InitializeComponent();
            GetWidgets();
            

        }

        private void DisplayWidgets()
        {
            foreach (Widget element in Widgets)
            {
                AddImageWidget(element);
                AddStatistic(element);
            }
        }

        private void AddStatistic(Widget widget)
        {
            TextBlock statWidget = new TextBlock();
            statWidget.Text = widget.userId.ToString();
            canvas.Children.Add(statWidget);
            Canvas.SetLeft(statWidget, widget.posX + 25);
            Canvas.SetTop(statWidget, widget.posY + 170);
        }

        private void AddImageWidget(Widget widget)
        {
            Image img = new Image();
            img.Source = new BitmapImage(new Uri("ms-appx:///Assets/logoEIP.png"));

            ToolTip toolTipNameWidget = new ToolTip();
            toolTipNameWidget.Content = widget.name;
            ToolTipService.SetToolTip(img, toolTipNameWidget);
            double y;
            double x;
            if (widget.posY + 162 > ((Frame)Window.Current.Content).ActualHeight)
            {
                y = ((Frame)Window.Current.Content).ActualHeight - 162;
            }
            else
            {
                y = widget.posY;
            }

            if (widget.posX + 162 > ((Frame)Window.Current.Content).ActualWidth)
            {
                x = ((Frame)Window.Current.Content).ActualWidth - 162;
            }
            else
            {
                x = widget.posX;
            }
            img.Margin = new Thickness(x, y, 0, 0);
            canvas.Children.Add(img);

        }

        private void SizePageChanged(object sender, SizeChangedEventArgs e)
        {
            canvas.Height = ((Frame)Window.Current.Content).ActualHeight;
            canvas.Width = ((Frame)Window.Current.Content).ActualWidth;
            canvas.UpdateLayout();

            if (AddWidget.Margin.Top > canvas.Height)
            {
                AddWidget.Margin = new Thickness(
                    AddWidget.Margin.Left,
                    canvas.Height - 32,
                    AddWidget.Margin.Right,
                    AddWidget.Margin.Bottom
                    );
            }
        }

        private async void GetWidgets()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri("http://localhost:8080/api/widgets");
            var accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImV4cCI6MTYwMTE5ODA5MywiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo0MjAwIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwIn0.6YBmpwtoEsECNvO2sRhkDtcH_gYocfKSFDh2AO7Hoy4";
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = "";

            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                Widgets = JsonConvert.DeserializeObject<Widget[]>(httpResponseBody);
                DisplayWidgets();
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        private async void ShowDialog_Click(object sender, RoutedEventArgs e)
        {
            AddWidgetDialog dialog = new AddWidgetDialog();
            var result = await dialog.ShowAsync();

            if (result == ContentDialogResult.Primary)
            {
                WidgetX = dialog.X;
                WidgetY = dialog.Y;
                Image img = new Image();
                img.Source = new BitmapImage(new Uri("ms-appx:///Assets/logoEIP.png"));

                canvas.Children.Add(img);
                Canvas.SetLeft(img, WidgetX);
                Canvas.SetTop(img, WidgetY);
            }
        }
    }

    public class Widget
    {
        public int id { get; set; }
        public string name { get; set; }
        public int posX { get; set; }
        public int posY { get; set; }
        public int userId { get; set; }
    }
}
